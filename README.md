# GCP LAB

![](https://github.com/GoogleCloudPlatform/microservices-demo/raw/main/docs/img/architecture-diagram.png)

the project contains the answer for the lab ["GCP_LAB"](https://m2-mosig-cloud.gitlab.io/lab_assignment/)

to easily navigate the project , you can use the following link map

- [Basic](#basic)
  - [Deploying the load generator on a local machine](#deploying-the-load-generator-on-a-local-machine)
  - [Deploying automatically the load generator in Google Cloud](#deploying-automatically-the-load-generator-in-google-cloud)
  - [Deploying the original application in GKE](#deploying-the-original-application-in-gke)
  - [Analyzing the provided configuration](#analyzing-the-provided-configuration)
  - [Targeting a minimal deployment](#targeting-a-minimal-deployment)
- [Advanced](#advanced)
  - [Canary release](#canary-release)
  - [Monitoring the application and the infrastructure](#monitoring-the-application-and-the-infrastructure)
  - [Performance evaluation](#performance-evaluation)
- [Bonus](#bonus)
  - [Autoscaling](#autoscaling)
  - [Deploying your own Kubernetes infrastructure](#deploying-your-own-kubernetes-infrastructure)
  - [Performance evaluation (Bonus)](#performance-evaluation-bonus)

## contact authors :

- [Othmane MDARHRI](mailto:othmane.mdarhri.1@gmail.com)
- [Hatim MOULINE](mailto:hatimmouline2014@gmail.com)
