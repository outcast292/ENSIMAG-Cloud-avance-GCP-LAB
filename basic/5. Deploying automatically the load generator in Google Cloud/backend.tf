terraform {
  backend "gcs" {
    credentials = "./creds/service-account.json"
    bucket  = "microservices-tf-state"
  }
}