resource "google_service_account" "default" {
  account_id   = "my-custom-sa"
  display_name = "Custom SA for VM Instance"
}

resource "google_compute_instance" "default" {
  name         = "my-instance-${count.index}"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"
  count = var.instances

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }

  metadata_startup_script = <<-EOF
  apt-get update -y 
  apt-get install python3-pip -y
  cd ~ 
  mkdir locust 
  cd locust 
  wget https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/main/src/loadgenerator/locustfile.py
  wget https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/main/src/loadgenerator/requirements.txt  
  pip install -r requirements.txt 
  export GEVENT_SUPPORT=True 
  python3 -m locust --host=http://${var.destIp} --headless -u ${var.users} 2>&1
  EOF

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.default.email
    scopes = ["cloud-platform"]
  }


}

# resource "google_storage_bucket_object" "locustfile" {
#   for_each = toset(["../src/loadgenerator/locustfile.py", "../src/loadgenerator/requirements.txt"])
#   name     = basename(each.key)
#   source   = each.value
#   bucket   = "microservices-tf-state"
# }

