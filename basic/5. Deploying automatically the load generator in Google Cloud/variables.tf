# variables.tf

variable "project" {
  description = "The name of the GCP project."
  type        = string
  default     = "practical-session-cloud"
}


variable "destIp" {
  description = "The address ip of the external service of the application"
  type        = string
}

variable "users" {
  description = "Number of users in locust"
  type        = number
  default = 10
}


variable "instances" {
  description = "Number of instances of  locust"
  type        = number
  default = 1
}