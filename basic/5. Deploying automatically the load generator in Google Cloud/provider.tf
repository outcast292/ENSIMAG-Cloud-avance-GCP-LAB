provider "google" {
  project     = "practical-session-cloud"
  region      = "us-central1"
  credentials = "${file("./creds/service-account.json")}"
}