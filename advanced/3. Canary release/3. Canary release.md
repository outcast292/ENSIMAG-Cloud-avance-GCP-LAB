# Canary releases

to achieve the canary release of out application , we started first by preparing a custom image with v2 , this image is available on dockerhub under : [dockerHub link](https://hub.docker.com/r/moulinehatim/frontend/tags)


First, we start by scaling up the frontend deployment up to 3 replicas

```bash
kubectl scale --replicas=3 deployment/frontend
```

then, we apply the following deployment , that uses the custom docker image we have created (The file is available in **release** folder : **deployment-v2.yml**)

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend-v2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
      annotations:
        sidecar.istio.io/rewriteAppHTTPProbers: "true"
    spec:
      serviceAccountName: default
      securityContext:
        fsGroup: 1000
        runAsGroup: 1000
        runAsNonRoot: true
        runAsUser: 1000
      containers:
        - name: server
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
                - ALL
            privileged: false
            readOnlyRootFilesystem: true
          image: moulinehatim/frontend:0.0.2
          ports:
            - containerPort: 8080
          readinessProbe:
            initialDelaySeconds: 10
            httpGet:
              path: "/_healthz"
              port: 8080
              httpHeaders:
                - name: "Cookie"
                  value: "shop_session-id=x-readiness-probe"
          livenessProbe:
            initialDelaySeconds: 10
            httpGet:
              path: "/_healthz"
              port: 8080
              httpHeaders:
                - name: "Cookie"
                  value: "shop_session-id=x-liveness-probe"
          env:
            - name: PORT
              value: "8080"
            - name: PRODUCT_CATALOG_SERVICE_ADDR
              value: "productcatalogservice:3550"
            - name: CURRENCY_SERVICE_ADDR
              value: "currencyservice:7000"
            - name: CART_SERVICE_ADDR
              value: "cartservice:7070"
            - name: RECOMMENDATION_SERVICE_ADDR
              value: "recommendationservice:8080"
            - name: SHIPPING_SERVICE_ADDR
              value: "shippingservice:50051"
            - name: CHECKOUT_SERVICE_ADDR
              value: "checkoutservice:5050"
            - name: AD_SERVICE_ADDR
              value: "adservice:9555"
            # # ENV_PLATFORM: One of: local, gcp, aws, azure, onprem, alibaba
            # # When not set, defaults to "local" unless running in GKE, otherwies auto-sets to gcp
            # - name: ENV_PLATFORM
            #   value: "aws"
            - name: ENABLE_PROFILER
              value: "0"
          # - name: CYMBAL_BRANDING
          #   value: "true"
          # - name: FRONTEND_MESSAGE
          #   value: "Replace this with a message you want to display on all pages."
          resources:
            requests:
              cpu: 100m
              memory: 64Mi
            limits:
              cpu: 200m
              memory: 128Mi
```

finally, we could run the following script that keeps loading the page , and greping it to check if it's version 1 or 2, then returns the percentage of times showing the version 2

```bash
requests=1000
count=$(for i in $(seq 1 $requests); do curl http://34.29.102.208/ -s; done | grep "VERSION 2" | wc -l)
percentage=$((100*count/requests))
echo "I got V2.0 $count times/$requests <=> $percentage%"

```
