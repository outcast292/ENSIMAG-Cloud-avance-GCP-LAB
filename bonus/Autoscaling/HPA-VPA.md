# AutoScalling [Bonus]

## HPA 

you can set up HPA by using the following command : 

```bash
kubectl apply -f HPA.yml 
```

to the following file HPA.yml :

```yaml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: hpa-frontend
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: frontend
  minReplicas: 1
  maxReplicas: 10
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 80
    - type: Resource
      resource:
        name: memory
        target:
          type: Utilization
          averageUtilization: 80
---
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: hpa-currency
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: currencyservice
  minReplicas: 1
  maxReplicas: 10
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 80
    - type: Resource
      resource:
        name: memory
        target:
          type: Utilization
          averageUtilization: 80
```

Then launch the massive traffic generator to see the magic.

## VPA 

Since, it’s not preinstalled in Kubernetes, we should start by installing it following Kubernetes instructions in their official repository : [here](https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler#install-command)

```bash
git clone https://github.com/kubernetes/autoscaler.git
./autoscaler/vertical-pod-autoscaler/hack/vpa-up.sh
```

Then make the Deployment replicas up to 2.

```bash
kubectl scale deployment frontend --replicas=2
kubectl scale deployment currencyservice --replicas=2
```

once all the pre-requisites are in place , you can set up VPA by using the following command : 
```bash
kubectl apply -f VPA.yml 
```

to the following file VPA.yml :

```yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: vpa-frontend
spec:
  targetRef:
    apiVersion: "apps/v1"
    kind:       Deployment
    name:       frontend
  updatePolicy:
    updateMode: "Auto" 
  resourcePolicy:
    containerPolicies:
    - containerName: "*" 
      controlledResources: ["cpu", "memory"]
      maxAllowed:
        cpu: "2000m"
        memory: "2Gi"
      minAllowed:
        cpu: "100m"
        memory: "64Mi"
---
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: vpa-currency
spec:
  targetRef:
    apiVersion: "apps/v1"
    kind:       Deployment
    name:       currencyservice
  updatePolicy:
    updateMode: "Auto"
  resourcePolicy:
    containerPolicies:
    - containerName: "*"
      controlledResources: ["cpu", "memory"]
      maxAllowed:
        cpu: "2000m"
        memory: "2Gi"
      minAllowed:
        cpu: "100m"
        memory: "64Mi"
```


Once again launch the massive traffic generator to see the magic.
