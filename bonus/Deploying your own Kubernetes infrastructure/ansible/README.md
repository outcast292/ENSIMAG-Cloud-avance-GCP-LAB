# Ansible

This directory gathers all the necessary files to run Ansible playbooks against the hosts and set up the Kubernetes cluster with the proper deployments for monitoring and the application itself.

## Contents
*   [Inventory](#inventory)
*   [Playbooks](#playbooks)

## Inventory

Ansible uses a dynamic inventory. Which means, using the gcp plugin that authenticates with credentials (available in the path specified in ansible.cfg), it retrieves information about the available instances and groups them according to their names and labels (The most important attributes for us).

## Playbooks

The directory playbooks contains the playbooks that has to be run on the instances. The hosts targetted are always the hosts that not only are part of the targetted group of instances (workers/master) but also haven't been provisionned yet. Those playbooks have been split according to the function we intend to set up.

*   **test.yml** A testing playbook to test the communication between Ansible and the instances

*   **all.yml** playbook that installs basic packages on all types of instances.
*   **kube_master_config.yml** playbook that configures the kubernetes master for orchestration and monitoring.
*   **scaling_master.yml** playbook that sets the environment variables and puts the scaling scripts in the node responsible for scaling the cluster : the master in our case.
*   **spark_master.yml** playbook sets up the working environment for spark in the node responsible for running the spark driver : the master in our case.
*   **join_cluster.yml** playbook that triggers the master (previously provisionned or not) to create the connection token that will allow the new workers to join the cluster.
*   **deploy_app.yml** playbook that triggers the master (previously provisionned or not) to create the connection token that will allow the new workers to join the cluster.

