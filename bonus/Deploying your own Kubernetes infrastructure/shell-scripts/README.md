# Scripts

Scripts shell used in the project for different purposes.

## Contents
*   [Start up Scripts](#start-up-scripts)
*   [Remote](#remote)
*   [Local](#local)

## Start up scripts

Those are scripts used previously to install required packages in the instances (master node and workers) upon booting. Those scripts have been translated to Ansible playbooks and, so, are no longer used. 
Ansible playbooks offer visibility about what succeeded and what failed and continues execution even though a certain task (equivalent to a command) failed. Thus, the migration.

## Remote

The directory remote contains scripts that are destined to be run inside the master node of the cluster. Those scripts are actually used to manage the cluster's scalability. 
(Only the ticked scripts are implemented and verified to be working)

*   [x] **scaling_up.sh** script that sends a sequence of curl commands to the Gitlab API in order to run the pipeline and add the desired number of instances to the cluster. That number is the first and only argument passed to the script.
*   [x] **scaling_down.sh** script that evicts the pods running inside of given workers (identified by their names), disconnects them from the cluster and then sends a sequence of curl commands to the Gitlab API to launch the scale-down job that destroys those workers and runs the necessary tasks to keep the state of the instances in Terraform valid for later scale up/down.
*   [ ] **suicide.sh** This script gently dismantles the cluster before notifying Gitlab to destroy all instances provisionned. 

## Local

The directory local contains scripts that are destined to be run by the Gitlab runners or any runner that will run a job of the pipeline. 
(Only the ticked scripts are implemented and verified to be working)

*   [x] **change_values.sh** scripts that notifies the Gitlab API from inside the runner to update the value of the number of workers. Actually, there is a Gitlab CI variable that keeps track of the number of the workers created by Terraform. This variable was intended to be used eventually to keep track of the workers that were created by Terraform but failed to be connected to the cluster and, thus, have gone rogue. 
*   [x] **delete_and_rename.sh** This scripts takes as an argument a literal string (that can be converted to an array) of the workers that we intend on destroying. It destroys those instances and then automatically gathers information from the terraform state to move the remaining workers to other indices in order to keep a valid sequence of workers exploitable for scale up again.
*   [x] **existing.sh** This scripts gathers information from the terraform state and reduces it to a json dictionnary where the key is the identifier of the instance and the value is its name.
