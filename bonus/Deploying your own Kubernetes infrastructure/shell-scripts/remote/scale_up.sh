#!/bin/bash

if [[ $# != 1 ]]
then
    echo "usage: scale_up.sh <nb_workers_to_add>"
else
    ####### Initialize pipeline
    #### Trigger pipeline and get its ID
    PIPELINE_ID=$(curl --location --request POST "https://gitlab.com/api/v4/projects/$PROJECT_ID/trigger/pipeline" \
        --form "ref=$TRIGGER_REF" \
        --form "token=$TRIGGER_TOKEN"| jq .id)
    #### Get ID of the new create-instances job
    JOB_ID=$(curl --location --request GET "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID/jobs" \
                --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" | jq -c '.[]' | SCALE_UP_JOB="$SCALE_UP_JOB" jq 'select(.name == 'env.SCALE_UP_JOB') | .id')

    ####### Run scale_up jobs : only adds workers
    curl --location --request POST "https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs/$JOB_ID/play" \
        --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
        --header 'Content-Type: application/json' \
        --data-raw "{
            \"job_variables_attributes\": [
                {
                    \"key\": \"NB_WORKERS_TO_ADD\",
                    \"value\": \"$1\"
                }
            ]
        }"
fi