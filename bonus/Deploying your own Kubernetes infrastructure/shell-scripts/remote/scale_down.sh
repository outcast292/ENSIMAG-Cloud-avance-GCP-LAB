#!/bin/bash

if [[ $# != 1 ]]
then
    echo "usage: <path-to-script>/scale_down.sh <list of names of the nodes to destroy>"
    echo "argument should be a literal string like : 'name-1 name-2 ...' "
else
    list_names=("$1")
    for node in $list_names
    do
        # Put the node into maintenance mode
        kubectl cordon $node
        # Drain the node
        kubectl drain $node --delete-local-data --force --ignore-daemonsets
        # Remove the node from the cluster
        kubectl delete node $node
    done
    #### Trigger pipeline and get its ID
    PIPELINE_ID=$(curl --location --request POST "https://gitlab.com/api/v4/projects/$PROJECT_ID/trigger/pipeline" \
        --form "ref=$TRIGGER_REF" \
        --form "token=$TRIGGER_TOKEN"| jq .id)
    #### Get ID of the new scale-down job
    JOB_ID=$(curl --location --request GET "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID/jobs" \
        --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" | jq -c '.[]' | SCALE_DOWN_JOB="$SCALE_DOWN_JOB" jq 'select(.name == 'env.SCALE_DOWN_JOB') | .id')
    ####### Run scale-down jobs
    curl --location --request POST "https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs/$JOB_ID/play" \
        --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
        --header 'Content-Type: application/json' \
        --data-raw "{
            \"job_variables_attributes\": [
                {
                    \"key\": \"list\",
                    \"value\": \"$1\"
                }
            ]
        }"
fi

