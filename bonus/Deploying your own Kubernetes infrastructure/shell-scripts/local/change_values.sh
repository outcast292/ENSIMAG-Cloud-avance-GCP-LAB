#!/bin/bash

if [[ $# != 1 ]]
then
    echo "usage: change_values.sh <current_nb_workers>"
else
    #### Change value of nb_workers
    curl --location --request PUT "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/variables/TF_VAR_NB_WORKERS?value=$1" \
        --header "PRIVATE-TOKEN: $PRIVATE_TOKEN"
fi