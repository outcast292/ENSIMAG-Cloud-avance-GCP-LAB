#!/bin/bash

if [[ $# != 1 ]]
then 
    echo $#
    echo "usage: delete_and_rename.sh <list of the names of the instances to delete>"
    echo "The first (and only) parameter should be a string that we can create an array from"
    exit 1
else  
    ## Determining the indices of the workers to be destroyed
    existing_workers=$(terraform show -json | jq .values.root_module.resources | jq -c '.[]' | jq 'select(.address | match("google_compute_instance.workers")) | .' | jq '{name: .values.name, address: .address, index: .index}')
    array=($1)
    names="["
    for item in "${array[@]}"; do
        names+="\"$item\","
    done
    names=${names%,} # remove trailing comma
    names+="]"
    to_destroy_indices=$(echo $existing_workers | jq --argjson names "$names" 'select(.name == $names[]) | .index')
    ## Deleting the workers and moving the remaining ones
    length=$(echo "$to_destroy_indices" | wc -w)
    if [[ $length != 0 ]]
    then
        existing_workers_length=$(echo $existing_workers | jq -c | wc -w)
        nb_workers_left=$(( $existing_workers_length - $length ))
        ## Indices of the workers that should be moved to allow future scale up
        potential_move_indices=$(echo $existing_workers | jq --argjson names "$names" 'select( .name as $n | $names | index($n) == null ) | .index ')
        to_move_indices=()
        for i in ${potential_move_indices[@]}; do
            if [[ $i -ge $nb_workers_left ]]; then
                to_move_indices+=("$i")
            fi
        done
        # Destroying and moving instances
        move=0
        for instance in $to_destroy_indices
        do 
            terraform destroy -auto-approve -target "google_compute_instance.workers[$instance]"
            if [[ $instance -lt $nb_workers_left ]] && [[ $move -lt ${#to_move_indices[@]} ]]
            then
                terraform state mv "google_compute_instance.workers[${to_move_indices[$move]}]" "google_compute_instance.workers[$instance]"
                move=$(( $move + 1 ))
            fi
        done
    fi
    echo $nb_workers_left > output.txt
fi