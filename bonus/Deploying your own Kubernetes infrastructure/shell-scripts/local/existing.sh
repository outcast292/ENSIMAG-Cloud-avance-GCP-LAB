#!/bin/bash

if [[ $# != 1 ]]; then
    echo "need argument : relative path to output file"
    exit 1
fi
terraform show -json | jq '.values.root_module.resources[] | select(.type == "google_compute_instance") | {(.address): .values.name}' | jq -s 'reduce .[] as $item ({}; . * $item)' > $1
