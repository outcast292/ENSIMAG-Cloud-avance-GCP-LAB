# Deploying your own Kubernetes infrastructure
This project is used to create the K8S infrastructure hosted by GCP. 

## Create an initial cluster
To establish an initial cluster, furnish the credentials for your GCP service account via the GitLab CI variable `SERVICEACCOUNT`. Ensure that this service account possesses the necessary permissions to create and oversee instances. Create a storage bucket with the designated name specified in `terraform/backend.tf` and link it to the service account. Terraform will store the remote state in this bucket, and failure may occur if it lacks access to the specified bucket.

For the initial creation of n workers, ensure that the variable `TF_VAR_NB_WORKERS` is set to n-2 before initiating the `create-instances` job. While typically, `TF_VAR_NB_WORKERS` signifies the number of existing workers, in this context, it can be employed to generate any desired quantity of workers initially.

once everything is inplace , you can use the gitlab pipeline to start the deployment .

## Clean
To dismantle all resources created by Terraform, execute the 'clean' job. Any 'clean' job from any pipeline will effectively carry out this task. Ensure to include the cleanup of the service account and the associated bucket to comprehensively remove all created elements.