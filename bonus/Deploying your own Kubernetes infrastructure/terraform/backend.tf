terraform {
    required_version = "~> 1.3.3"
    backend "gcs" {
        credentials = "../creds/serviceaccount.json"
        bucket      = "terraform_remote_state_v2"
    }
}