#######################################################################################################
#################### Generate an SSH key pair #########################################################
terraform {
  required_providers {
    tls = {
      source = "hashicorp/tls"
    }
  }
}

provider "tls" {
  // no config needed
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# This block makes all resources have the private key
resource "google_compute_project_metadata" "default" {
  metadata = {
    ssh-keys = "${var.SSH_USER}:${tls_private_key.ssh.public_key_openssh} ${var.SSH_USER}"
  }
}

resource "local_sensitive_file" "ssh_private_key_openssh" {
  content         = tls_private_key.ssh.private_key_pem
  filename        = var.PRIVATE_KEY
  file_permission = "0600"
}

resource "local_sensitive_file" "ssh_public_key_openssh" {
  content         = tls_private_key.ssh.public_key_openssh
  filename        = "${var.PRIVATE_KEY}.pub"
  file_permission = "0644"
}

#######################################################################################################
#################### Create a network with a firewall #################################################
resource "google_compute_network" "vpc_network" {
  project = var.project
  name    = "vpc-network"
}

resource "google_compute_firewall" "internal" {
  name    = "internal"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }
  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }
  target_tags   = ["allow-ssh", "allow-http", "allow-https"]
  source_ranges = ["0.0.0.0/0"]
}

locals {
  # This file should be created by running jq filters on 'terraform show -json'
  # terraform show -json | jq '.values.root_module.resources[] | select(.type == "google_compute_instance") | {(.address): .values.name}' | jq -s 'reduce .[] as $item ({}; . * $item)' > ./existing_workers.json
  # It's a json encoded map where the key is the address of the ressource and the value its name
  existing_resources = jsondecode(file(var.PATH_TO_EXISTING))
}

#######################################################################################################
#################### Create GCE instances : kubernetes masters ########################################

resource "google_compute_disk" "storage_disk" {
  name = "nfs-disk"
  type = "pd-standard"
  zone = var.zone
  size = "50"
}


resource "google_compute_instance" "masters" {
  count        = var.NB_MASTERS
  name         =  "master-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone
  tags         = ["allow-ssh", "allow-http", "allow-https"]

  boot_disk {
    initialize_params {
      image = var.image
      size  = var.size
    }
  }

  attached_disk {
    source      = google_compute_disk.storage_disk.self_link
    device_name = google_compute_disk.storage_disk.name
  }

  # Labels the instances to indicate which ones existed before the plan and which ones didn't
  labels = {
    provisionned = lookup(local.existing_resources, "google_compute_instance.masters[${count.index}]", "") != ""
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

#######################################################################################################
#################### Create GCE instances : kubernetes workers ########################################

resource "google_compute_instance" "workers" {
  count        = var.NB_WORKERS
  name         = lookup(local.existing_resources, "google_compute_instance.workers[${count.index}]", "worker-${uuid()}")
  machine_type = var.machine_type
  zone         = var.zone
  tags         = ["allow-ssh"]
  boot_disk {
    initialize_params {
      image = var.image
      size  = var.size
    }
  }

  # Labels the instances to indicate which ones existed before the plan and which ones didn't
  labels = {
    provisionned = lookup(local.existing_resources, "google_compute_instance.workers[${count.index}]", "") != ""
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}
