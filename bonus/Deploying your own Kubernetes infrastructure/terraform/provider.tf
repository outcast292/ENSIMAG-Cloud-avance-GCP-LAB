provider "google" {
   credentials = "${file("../creds/serviceaccount.json")}"
   project     = "sdtd-v2"
   region      = "europe-west9"
   zone        = "europe-west9-a"
}