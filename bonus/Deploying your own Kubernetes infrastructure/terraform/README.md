# Provisioning
The files in this directory are used to provision resources in GCP. 
As the job 'create-instances' in the pipeline indicates, the provisioning goes through three steps:

## Initialisation
By running the command 'terraform init', terraform checks if we can provision resources from the provider by checking the validity of the credentials provided, the rights allowed through those credentials ... Then it creates an auth token to be used for later steps. The token is typically store in a tmp file, which means you need to run terraform init everytime you intend on using terraform in a new docker.

## Planification
By running the command 'terraform plan', terrafrom generates the plan of the changes that should be done to set up the current infrastructure (resources to be deleted/added/modified). 

For the variables declared in the file 'variables.tf',terraform by default uses the values specified in the file 'terrafom.tfvars'. Those values can be overriden through the command line. 

Actually, the number of workers (and masters even though we don't manage the creation of multiple masters) and the size are defined as numerical values. To override the default values, we were intending on using the command line in order to pass the values. But, we found out that Terraform interprets those arguments as strings (even though we declare them as integers before running the command). So Terraform crashes since the type given doesn't match the type declared in the file. That's why in the .gitlab-ci.yml file we concatenate the desired values to the terrafrom.tfvars file before creating the plan.

## Application
By running the command 'terraform apply', terraform applies the changes planned as stated in the planfile. After applying the job Terraform updates the state in the state file store in the backend (basically the bucket where the remote state is stored)

Warning: Some sensitive data could be revealed in this stage !