variable "zone" {
    description = "The zone where we intend on provisioning the VMs"
    type = string
    nullable = false
}

variable "project" {
    description = "The project ID as specified in GCP"
    type = string
    nullable = false
    # sensitive = true
}

variable "machine_type" {
    description = "The machine type gives hardware specifications to the provider"
    type = string
    nullable = false
}

variable "image" {
    description = "The OS used in our VMs (to be uploaded)"
    type = string
    nullable = false
}

variable "size" {
    description = "The storage allowed in a VM"
    type = number
}

variable "SSH_USER" {
    description = "The user that we can ssh to in the GCP instances"
    type = string
}

variable "PRIVATE_KEY" {
    description = "Relative path (from home) to the ssh private key file"
    type = string
}

variable "NB_WORKERS" {
    description = "The number of workers to provision for the cluster"
    type = number
    default = 2
}

variable "NB_MASTERS" {
    description = "Number of the kubernetes masters in the cluster"
    type = number
    default = 1  
}

variable "PATH_TO_EXISTING" {
    description = "Relative path to the json file containing a map of the existing instances"
    type = string
    nullable = false
    default = "existing_instances.json"
}