# Performance evaluation [Bonus]

## Monitor redis

in order to monitor redis , we have to make the redis metrics available, luckily , an exporter made for redis is already available on github. 

first, we have to deploy under the monitoring namespace  a new deployment for the redis-exporter , while also precising the env variable REDIS_ADDR with the service addresse of the redis instance.


```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-exporter
  namespace: monitoring
spec:
  replicas: 1
  selector:
    matchLabels:
      app: redis-exporter
  template:
    metadata:
      labels:
        app: redis-exporter
    spec:
      containers:
        - name: redis-exporter
          image: oliver006/redis_exporter
          ports:
            - containerPort: 9121
              name: exporter-port
          env:
            - name: REDIS_ADDR
              value: "redis-cart.default.svc.cluster.local"
```

then we have to deploy a service for our redis-exporter so we can make it available on 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: redis-exporter
  namespace: monitoring
  labels:
    app: redis-exporter
    release: prometheus
spec:
  selector:
    app: redis-exporter
  ports:
    - name: exporter-port
      protocol: TCP
      port: 9121
      targetPort: 9121
```
finally , we have to create  serviceMonitor, it is a custom ressource used to define an application you wish to scrape metrics from within Kubernetes, the controller will action the ServiceMonitors we define and automatically build the required Prometheus configuration.
Within the ServiceMonitor we specify the Kubernetes Labels that the Operator can use to identify the Kubernetes Service which in turn then identifies the Pods, that we wish to monitor.  for our case we specified the label app "redis-exporter".

it's also important to specify add the release label to the service monitor , as we have configured our prometheus to be able to discover all serviceMonitors that contain this tag.

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
    app: prometheus-redis-exporter
    release: prometheus
  name: redis-exporter-prometheus
  namespace: monitoring  
spec:
  endpoints:
    - interval: 15s
      port: exporter-port
  selector:
    matchLabels:
      app: redis-exporter 

```

## Alerts 

a lot of alerts were set in place using the helm chart, the report can provide you with more details.

![](../../assets/imgs/img_3.png)

![](../../assets/imgs/img_4.png)